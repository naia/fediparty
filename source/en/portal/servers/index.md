---
layout: "post"
title: "Themed servers"
---

**Unsure where to register to join Fediverse?** Choose a website from this curated list. It includes [Mastodon](/en/mastodon), [Pleroma](/en/pleroma), [Friendica](/en/friendica), [Misskey](/en/misskey) and [Hubzilla](/en/hubzilla) servers. Websites are not restricted to their theme. They help people with common interests find their community.

<ul class="article-list">

### 💡 [Sciences](#sciences)
* [lugnasad.eu](https://lugnasad.eu) - for French speaking science lovers *(Friendica)*

### 🎨 [Humanities](#humanities)
* [oulipo.social](https://oulipo.social) - a lipogrammatic server *(Mastodon)*

### 🎓 [Education](#education)
* [tusk.schoollibraries.net](https://tusk.schoollibraries.net) - helping educators improve the School Libraries Resource Network *(Mastodon)*
* [mastodon.oeru.org](https://mastodon.oeru.org) - for educators and learners involved in the [OERu](https://oeru.org)

### 🎵 [Music](#music)
* [koreadon.com](https://koreadon.com) - for K-POP music fans *(Mastodon)*
* [feedbeat.me](https://feedbeat.me) - dedicated to culture and events such as music, poetry, comedy (German) *(Mastodon)*

### 🔭 [Interests and hobbies](#hobbies)
* [screenwriting.space](https://screenwriting.space) - a place for storytellers *(Mastodon)*
* [rollenspiel.social](https://rollenspiel.social) - (German) roleplay, Pen & Paper, tabletop, TCG, for all gamers *(Mastodon)*
* [rollenspiel.group](https://rollenspiel.group) - (German) roleplay, Pen & Paper, tabletop, TCG, for all gamers *(Friendica)*
* [radiosocial.de](https://radiosocial.de) - for German radio amateurs *(Mastodon)*
* [podcastindex.social](https://podcastindex.social) - for stake holders of podcasting who are interested in improving the ecosystem *(Mastodon)*
* [solarpunks.social](https://solarpunks.social) - decentralized network for solarpunks *(Mastodon)*


### 🚄  Travel, Transport and Infrastructure
* [rail.chat](https://rail.chat) - discussions about long-distance, passenger and freight rail networks for economic, environmental and equity benefits *(Mastodon)*

### 🎏 [Language specific](#languages)
* [toki.social](https://toki.social) - for those interested in toki pona *(Mastodon)*
* [mastodon.sk](https://mastodon.sk) - for Slovak users *(Mastodon)*
* [mastodon.eus](https://mastodon.eus) - for Euskera/Basque speakers *(Mastodon)*
* [xarxa.cloud](https://xarxa.cloud) - for Catalan and Spanish speakers *(Mastodon)*
* [mastodon.fedi.bzh](https://mastodon.fedi.bzh) - for Breton and Gallo speakers *(Mastodon)*
* [gomastodon.cz](https://gomastodon.cz) - for Czech users *(Mastodon)*
* [soc.phreedom.club](https://soc.phreedom.club) - for Russian users *(Pleroma)*
* [librosphere.fr](https://librosphere.fr) - for French speakers *(Pleroma)*

### ⛺ [Regional](#regional)
* [muenchen.social](https://muenchen.social) - Munich, Germany *(Mastodon)*
* [mastodon.bayern](https://mastodon.bayern) - Bavaria, Germany *(Mastodon)*
* [ruhr.social](https://ruhr.social) - Ruhr area, Germany *(Mastodon)*
* [social.cologne](https://social.cologne) - Cologne, Germany *(Mastodon)*
* [nrw.social](https://nrw.social) - North Rhine-Westphalia, Germany *(Mastodon)*
* [social.saarland](https://social.saarland) - Saarland, Germany *(Mastodon)*
* [machteburch.social](https://machteburch.social) - Magdeburg, Germany *(Mastodon)*
* [brandenburg.social](https://brandenburg.social) - Brandenburg, Germany *(Mastodon)*
* [berlin.social](https://berlin.social) - Berlin, Germany *(Mastodon)*
* [osna.social](https://osna.social) - Osnabrück, Germany *(Mastodon)*
* [fem.social](https://fem.social) - Ilmenau, Germany *(Mastodon)*
* [friendica.a-zwenkau.de](https://friendica.a-zwenkau.de) - Zwenkau, Germany *(Friendica)*
* [chaotic.social](https://chaotic.social) - Hannover, Germany *(Mastodon)*
* [frankfurt.social](https://frankfurt.social) - Frankfurt, Germany *(Mastodon)*
* [harz.social](https://harz.social) - Harz area, Germany *(Mastodon)*
* [rheinneckar.social](https://rheinneckar.social) - Rhein-Neckar area, Germany *(Mastodon)*
* [nahe.social](https://nahe.social) - Nahe region, Germany *(Mastodon)*
* [moessingen.social](https://moessingen.social) - Mössingen area, Germany *(Mastodon)*
* [graz.social](https://graz.social) - Graz, Austria *(Mastodon)*
* [krems.social](https://krems.social) - Krems, Austria *(Mastodon)*
* [fedi.at](https://fedi.at) - Austria *(Mastodon)*
* [oslo.town](https://oslo.town) - Oslo, Norway *(Mastodon)*
* [norrebro.space](https://norrebro.space) - Denmark *(Mastodon)*
* [mastodon.nl](https:/mastodon.nl) - Netherlands *(Mastodon)*
* [mastodon.scot](https://mastodon.scot) - Scotland, or who identify as Scottish *(Mastodon)*
* [glasgow.social](https://glasgow.social) - Glasgow, Scotland *(Mastodon)*
* [bath.social](https://bath.social) - Bath, UK *(Mastodon)*
* [socialclub.nyc](https://socialclub.nyc) - New York, USA *(Mastodon)*
* [sfba.social](https://sfba.social) - San Francisco Bay Area, USA *(Mastodon)*
* [leafposter.club](https://leafposter.club) - Canada *(Pleroma)*
* [mastodon.quebec](https://mastodon.quebec) - Quebec, Canada *(Mastodon)*
* [mastodon.ml](https://mastodon.ml) - Russia *(Mastodon)*
* [mstdn.es](https://mstdn.es) - Spain *(Mastodon)*
* [barcelona.social](https://barcelona.social) - Barcelona *(Pleroma)*
* [mastodon.in.th](mastodon.in.th) - Thailand *(Mastodon)*
* [social.net.ua](https://social.net.ua) - Ukraine *(Pleroma)f*
* [aus.social](https://aus.social) - Australia *(Mastodon)*
* [mastodon.green](https://mastodon.green) - green European Union *(Mastodon)*
* [turkum.site](https://turkum.site) - Turkey *(Mastodon)*

### 🎬 [Book / Game / Show theme](#entertainment)
* [cmdr.social](https://cmdr.social) - dedicated to Elite: Dangerous and space exploration *(Mastodon)*
* [osrs.club](https://osrs.club) - themed around Old School Runescape *(Pleroma)*

### 🐧 [For techies](#servers-for-techies)
* [fsmi.social](https://fsmi.social) - free software movement of India *(Mastodon)*
* [indieweb.social](https://indieweb.social) - for participants and supporters of the IndieWeb movement *(Mastodon)*
* [tmkis.social](https://tmkis.social) - webhosting and internet services *(Mastodon)*

### 💻 [Programming](#instances-for-programmers)
* [pythondevs.social](https://pythondevs.social) - for Python developers of all experience levels *(Pleroma)*
* [pythonverse.social](https://pythonverse.social) - for  Python developers *(Mastodon)*


### 🔴 [Political and social views](#political-and-social-views)
* [hispagatos.space](https://hispagatos.space) - for hackers, social anarchists, and anarchist hackers *(Mastodon)*
* [campaign.openworlds.info](https://campaign.openworlds.info) - for campaigners and NGOs *(Mastodon)*
* [ontological.city](https://ontological.city) - for debating on philosophy, society, politics, degrowth and other controversial ideas *(Mastodon)*

### 🐰 [Ecology and animals](#ecology-and-animals)
* [toot.cat](https://toot.cat) - instance for cats, the people who love them, and kindness in general *(Mastodon)*
* [climatejustice.social](https://climatejustice.social) - for activists of the global climate justice and social justice movement *(Mastodon)*

### 👽 [Fandoms](#fandoms)
* [bungle.online](https://bungle.online) - fandom instance, post media, fan art and fanfiction *(Misskey)*

### 🐾 [Subcultures](#subcultures)
* [this.mouse.rocks](https://this.mouse.rocks) - furry instance for all the mouse adjacent folks who rock *(Mastodon)*
* [birds.garden](https://birds.garden) - a quiet, peaceful place where any bird can find refuge *(Pleroma)*
* [stop.voring.me](https://stop.voring.me) - fun hangout for furries and normies alike *(Misskey)*

### 🌏 [Notable generalistic](#notable-generalistic)
// *small-to-medium sized instances that will be happy to have new users*
* [appalachian.town](https://appalachian.town)
* [venera.social](https://venera.social) - friendly humans are welcome *(Friendica)*
* [nerdculture.de](https://nerdculture.de) *(Mastodon)*
* [ieji.de](https://ieji.de) - has Tor .onion address *(Mastodon)*
* [misskey.de](https://misskey.de) - hosted in Helsinki *(Misskey)*
* [wienermobile.rentals](https://wienermobile.rentals) - for all reasonable people *(Pleroma)*
* [howlr.me](https://howlr.me) - For members of niche and alternative lifestyle communities. Originally alterhuman focused. *(Akkoma)*

### 🐚 [Run by tech-savvy organizations](#run-by-tech-savvy-organizations)
* [en.osm.town](https://en.osm.town) - for the OpenStreetMap Community *(Mastodon)*
* [swiss-chaos.social](https://swiss-chaos.social) - Chaos Computer Club Switzerland *(Mastodon)*

### 🎉 [Notable mention](#notable-mention)
// *are open to particular audience, i.e., to students of university*
* [mastodon.mit.edu](https://mastodon.mit.edu) - for the MIT community *(Mastodon)*
* [mastodon.acc.sunet.se](https://mastodon.acc.sunet.se) - by Academic Computer Club at Umeå University, Sweden *(Mastodon)*
* [metu.life](https://metu.life) - for METU university of Turkey *(Mastodon)*

### 🎉 [Various (registration by application)](#registration-by-application)
// *these servers may be removed in future automated updates due to signups by application*
* [augsburg.social](https://augsburg.social) - Augsburg, Germany *(Mastodon)*
* [wue.social](https://wue.social) - Würzburg (and the neighbourhood), Germany *(Mastodon)*
* [bonn.social](https://bonn.social) - Bonn, Germany *(Mastodon)*
* [krefeld.life](https://krefeld.life) - Krefeld, Germany *(Mastodon)*
* [dresden.network](https://dresden.network) - Dresden, Germany *(Mastodon)*
* [darmstadt.social](https://darmstadt.social) - Darmstadt, Germany *(Mastodon)*
* [fulda.social](https://fulda.social) - Fulda, Germany *(Mastodon)*
* [norden.social](https://norden.social) - Northern Germany *(Mastodon)*
* [voi.social](https://voi.social) - Austria *(Mastodon)*
* [wien.rocks](https://wien.rocks) - Vienna, Austria *(Mastodon)*
* [snabelen.no](https://snabelen.no) - Norway *(Mastodon)*
* [mastodon.se](https://mastodon.se) - Sweden *(Mastodon)*
* [tukkers.online](https://tukkers.online) - Twente region, Netherlands *(Mastodon)*
* [mastodon.opencloud.lu](https://mastodon.opencloud.lu) - Luxembourg (private and public organisations) *(Mastodon)*
* [mastodon.uy](https://mastodon.uy) - Uruguay *(Mastodon)*
* [chilemasto.casa](https://chilemasto.casa) - Chile *(Mastodon)*
* [mastodon.ie](https://mastodon.ie) - Ireland *(Mastodon)*
* [mastodon.org.uk](https://mastodon.org.uk) - UK *(Mastodon)*
* [mastodon.me.uk](https://mastodon.me.uk) - UK tech community *(Mastodon)*
* [social.tulsa.ok.us](https://social.tulsa.ok.us) - Tulsa, Northeast Oklahoma, USA *(Mastodon)*
* [kcmo.social](https://kcmo.social) - Kansas City, USA *(Mastodon)*
* [mastodos.com](https://mastodos.com) - Kyoto, Japan *(Mastodon)*
* [mastodon.madrid](https://mastodon.madrid) - Madrid, Spain *(Mastodon)*
* [masto.pt](https:///masto.pt) - Portugal *(Mastodon)*
* [scicomm.xyz](https://scicomm.xyz) - for scientists and science enthusiasts *(Mastodon)*
* [fediscience.org](https://fediscience.org) - for publishing scientists, from natural sciences to the humanities *(Mastodon)*
* [writing.exchange](https://writing.exchange) - community for poets, writers, bloggers *(Mastodon)*
* [imaginair.es](https://imaginair.es) - for writers, painters, cartoonists and people with imagination *(Mastodon)*
* [mograph.social](https://mograph.social) - for motion design community, VFX / 3D artists, animators, illustrators *(Mastodon)*
* [socel.net](https://socel.net) - for animation professionals and freelancers *(Mastodon)*
* [history.lol](https://history.lol) - for history and history jokes *(Mastodon)*
* [iztasocial.site](https://iztasocial.site) - discussions about psychology for professors of SUAyED Psicología  university of Mexico *(Mastodon)*
* [openbiblio.social](https://openbiblio.social) - German libraries and information facilities *(Mastodon)*
* [ausglam.space](https://ausglam.space) - Australian galleries, libraries, archives, museums and records people *(Mastodon)*
* [linernotes.club](https://linernotes.club) - for discussing music recordings *(Mastodon)*
* [bostonmusic.online](https://bostonmusic.online) - digital space serving the Boston music scene *(Mastodon)*
* [tabletop.social](https://tabletop.social) - for tabletop gamers
* [mastodon.radio](https://mastodon.radio) - for Amateur (Ham) Radio community *(Mastodon)*
* [kith.kitchen](https://kith.kitchen) - food and cooking *(Mastodon)*
* [tooot.im](https://tooot.im) - main instance language is Hebrew *(Mastodon)*
* [toot.si](https://toot.si) - Slovenian (multilingual) instance *(Mastodon)*
* [mastodontti.fi](https://mastodontti.fi) - Finnish, public toots should be in Finnish *(Mastodon)*
* [floss.social](https://floss.social) - for people who support or build Free Libre Open Source Software *(Mastodon)*
* [linuxrocks.online](https://linuxrocks.online) - dedicated to Linux and technologies *(Mastodon)*
* [mathstodon.xyz](https://mathstodon.xyz) - for Maths people *(Mastodon)*
* [digipres.club](https://digipres.club) - conversations about digital preservation *(Mastodon)*
* [phpc.social](https://phpc.social) - PHP community
* [eupublic.social](https://eupublic.social) - for people interested in a united, post-national, truly democratic Europe *(Mastodon)*
* [dads.cool](https://dads.cool) - anyone with a kid can be a dad *(Mastodon)*
* [tech.lgbt](https://tech.lgbt) - for tech workers, academics, students, and others in tech who are LGBTQA+ allies *(Mastodon)*
* [colorid.es](https://colorid.es)- Spanish LGBTQIA+ / queer instance primarily for Portuguese speakers *(Mastodon)*
* [notbird.site](https://notbird.site) - left-wing instance of hackers, makers and gays *(Mastodon)*
* [fandom.ink](https://fandom.ink) - for fans and fandoms of all types *(Mastodon)*
* [aspiechattr.me](https://aspiechattr.me) - for aspies and other NDs where you can chat to other peeps with ASD, ADHD, and related conditions *(Mastodon)*
* [meow.social](https://meow.social) - general furry/safe-space instance *(Mastodon)*
* [tooting.ch](https://tooting.ch) - generic instance hosted by the FairSocialNet association *(Mastodon)*
* [weirder.earth](https://weirder.earth) - for thoughtful weirdos *(Hometown-Mastodon)*
* [m.g3l.org](https://m.g3l.org) - by G3L, libre software association *(Mastodon)*
* [digitalcourage.social](https://digitalcourage.social) - by DigitalCourage *(Mastodon)*
* [mastodon.nzoss.nz](https://mastodon.nzoss.nz) - by New Zealand Open Source Society *(Mastodon)*
* [mastodon.roflcopter.fr](https://mastodon.roflcopter.fr) - by Roflcopter, [CHATONS](https://chatons.org) member *(Mastodon)*
* [libretooth.gr](https://libretooth.gr) - by LibreOps, who contribute to (re-)decentralizing the net *(Mastodon)*
* [photog.social](https://photog.social) - place for your photos *(Mastodon)*
* [mastodon.cc](https://mastodon.cc) - for art *(Mastodon)*
* [theres.life](https://theres.life) - for Christians *(Mastodon)*
* [mastodon.ocf.berkeley.edu](https://mastodon.ocf.berkeley.edu) - for Berkeley students, faculty, and staff *(Mastodon)*
* [mastodon.utwente.nl](https://mastodon.utwente.nl) - for the University of Twente community *(Mastodon)*
* [fikaverse.club](https://fikaverse.club) - Swedish and other Scandinavian languages *(Mastodon)*
* [scholar.social](https://scholar.social) - for researchers, undergrads, journal editors, librarians, administrators *(Mastodon)*
* [literatur.social](https://literatur.social) - for German speaking authors and literary people *(Mastodon)*
* [dobbs.town](https://dobbs.town) - open to members of The Church of the SubGenius *(Mastodon)*
* [wpbuilds.social](https://wpbuilds.social) - for those who love Wordpress and open source *(Mastodon)*
* [feuerwehr.social](https://feuerwehr.social) - for all firefighters in German-speaking countries *(Mastodon)*
* [dragonscave.space](https://dragonscave.space) - instance with a blind and visually impaired membership, welcome to everyone who is a nice person *(Mastodon)*
* [mastodon.gougere.fr](https://mastodon.gougere.fr) - Auxerre, France *(Mastodon)*
* [stereodon.social](https://stereodon.social) - self-managed social network devoted to underground music (Italian, English languages) *(Mastodon)*
* [solarpunk.moe](https://solarpunk.moe) - for solarpunk nerds *(Mastodon)*
* [leftist.network](https://leftist.network) - for those with Leftist politics *(Mastodon)*
* [kansas-city.social](https://kansas-city.social) - Kansas City, USA *(Mastodon)*
* [PCGamer.social](https://pcgamer.social) - by the PC gamer, for the PC gamer *(Mastodon)*
* [poliverso.org](https://poliverso.org) - dedicated to politics and digital rights (Italian speaking) *(Friendica)*
* [rheinland.social](https://rheinland.social) - Rheinland, Germany *(Mastodon)*
* [editors.place](https://editors.place) - for developmental, line, technical, and copy editors *(Mastodon)*
* [social.politicaconciencia.org](https://social.politicaconciencia.org) - community about politics, science and culture for Spanish speakers *(Mastodon)*
* [andalucia.social](https://andalucia.social) - Andalucia, Spain *(Mastodon)*
* [polyglot.city](https://polyglot.city) - for multilingual and polyglots *(Mastodon)*
* [esperanto.masto.host](https://esperanto.masto.host) - for those interested in Esperanto *(Mastodon)*
* [masto.nobigtech.es](https://masto.nobigtech.es) -  for Spanish speakers *(Mastodon)*
* [triangletoot.party](https://triangletoot.party) - Triangle region of North Carolina, USA *(Mastodon)*
* [sf.social](https://sf.social) - San Francisco Bay Area, USA *(Mastodon)*
* [techlover.eu](https://techlover.eu) - talk about new technologies like development, digital art or science *(Mastodon)*
* [ruby.social](https://ruby.social) - for people interested in Ruby, Rails and related topics *(Mastodon)*
* [pcgamer.social](https://pcgamer.social) - by PC gamer, for PC gamers *(Mastodon)*
* [activism.openworlds.info](https://activism.openworlds.info) - for activists *(Mastodon)*
* [rizomatica.org](https://rizomatica.org) - Italian speaking community that values social emancipation and equality *(Hubzilla)*
* [pipou.academy](https://pipou.academy) - French community with emphasis on kindness *(Mastodon)*

## Disclaimer
`Following instances skipped`: number of users > 5.000, closed registration *at the moment of checking*, behind Cloudflare, running old code, theme / description not understood due to language barrier or scarce description, long blocklists, experimental servers.

__Note__: new server additions welcome, however, this is a curated list for newcomers - for this reason second-level, human-friendly domain names will more likely be accepted than 3-level, long, unintelligible strings of letters.

__Note__: servers with closed registration may be placed only in "Notable mention" category, only if they represent well-known organizations or establishments.

Add your own themed instance via [merge request](https://codeberg.org/fediverse/fediparty/src/branch/main/source/en/portal/servers/index.md) or send a suggestion to [@lightone@mastodon.xyz](https://mastodon.xyz/@lightone) via Fediverse. If your server already has many users, it will be nice of you to let other servers grow, for a healthier decentralization.
</ul>

## 🌟 Other research links
- [Pirate Parties in Fediverse](https://codeberg.org/lostinlight/distributopia/src/branch/main/caramba)
- [Mastodon server distribution](https://chaos.social/@leah/99837391793032137) research by @Leah

